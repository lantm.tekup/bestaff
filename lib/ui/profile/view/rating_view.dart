import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

typedef void RatingChangeCallback(double rating);
class RatingView extends GetView {
  final int starCount;
  final double rating;
  final RatingChangeCallback onRatingChanged;
  final Color color;

  RatingView(
      {this.starCount = 5, this.rating = .0, this.onRatingChanged, this.color});

  Widget _buildStar(BuildContext context, int index) {
    Widget icon;
    if (index >= rating) {
      icon = ImageCustomized(path: ic_star_no_fill, width: 44, height: 44);
    } else if (index > rating - 1 && index < rating) {
      icon = new Icon(
        Icons.star_half,
        color: color ?? Theme.of(context).primaryColor,
        size: 44,
      );
    } else {
      icon = ImageCustomized(path: ic_star_filled, width: 44, height: 44);
    }
    return new InkResponse(
      onTap:
          onRatingChanged == null ? null : () => onRatingChanged(index + 1.0),
      child: icon,
    );
  }

  @override
  Widget build(BuildContext context) {
    var list = [bad_text, least_text, so_good_text, good_text, great_text];
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: new List.generate(
                  starCount, (index) => Expanded(child: _buildStar(context, index)))),
          SizedBox(height: 6,),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: list.map((e) => Expanded(
              child: TextCustomized(
                text: e,
                color: ColorHelper.parseColor("#6B6B6B"),
                size: mediumSize,
                isCenter: true,
              ),
            )).toList()
          )
        ],
      ),
    );
    // return Row(
    //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //   children: list
    //       .map((e) => Column(
    //             children: [
    //               buildStar(context, e.indexOf(e)),
    //               TextCustomized(
    //                 text: e,
    //                 color: ColorHelper.parseColor("#6B6B6B"),
    //                 size: mediumSize,
    //               )
    //             ],
    //           ))
    //       .toList(),
    // );
  }
}
