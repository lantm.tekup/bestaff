import 'dart:io';

import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/profile_repositories.dart';
import 'package:be_staff/data/request/update_profile_request.dart';
import 'package:be_staff/data/response/detail_user_info.dart';
import 'package:be_staff/data/response/experience_response.dart';
import 'package:be_staff/ui/profile/contract/profile_contract.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ProfileController extends GetxController implements ProfileContract{
  File avatarFile;
  ProfileRepositories repositories;
  ProfileContract contract;
  UpdateProfileRequest updateProfileRequest;
  String pathAvatar;
  String email;

  //Info user
  DetailUserInfo mInfoUser;

  //Info experience
  List<Experience> mExperience;
  List<Experience> mDegree;

  //For basic info page
  TextEditingController mobileController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  //For tab bar
  bool isOtherTabBar = false;
  
  //For show position of user
  String userPosition;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    repositories = Injector().onGetProfile;
    contract = this;
    onGetInfoUser();
    onGetExperience();
  }

  void onUpdateStateTabBar(bool value) {
    isOtherTabBar = value;
    update();
  }

  Future<void> onEditAvatar({bool isFromCamera}) async {
    File _file = await onSinglePickerImage(
        isFromCamera: isFromCamera, imageQuality: 100);
    if (Get.isBottomSheetOpen) {
      Get.back();
    }
    if (_file != null) {
      avatarFile = _file;
      update();
    }
  }

  Future<File> onSinglePickerImage(
      {bool isFromCamera, int imageQuality}) async {
    PickedFile pickedFile = await ImagePicker().getImage(
        source: isFromCamera ? ImageSource.camera : ImageSource.gallery,
        imageQuality: imageQuality);
    return File(pickedFile.path);
  }

  void onUploadAvatar () {
    repositories.onUploadAvatar(avatarFile).then((value) {
      return contract.onUploadAvatarSuccess(value);
    }).catchError((onError){
      return contract.onError(onError);
    });
  }

  void onUpdateRequest() {
    updateProfileRequest = UpdateProfileRequest();
    updateProfileRequest.avatar = pathAvatar;
    updateProfileRequest.email = email ?? StorageInfoUser().getInfoUser.email;
  }

  void onUpdateProfile () {
    repositories.onUpdateProfile(updateProfileRequest).then((value) {
      return contract.onUpdateProfileSuccess(value);
    }).catchError((onError){
      return contract.onError(onError);
    });
  }

  void onPressedSave() {
    if(avatarFile != null){
      Get.dialog(LoadingDialog());
      onUploadAvatar();
    }
  }

  void onGetInfoUser() {
    repositories.onGetDetailInfoUser().then((value) {
      return contract.onGetInfoUserSuccess(value);
    }).catchError((onError){
      return contract.onError(onError);
    });
  }

  void onGetExperience(){
    repositories.onGetExperience().then((value) {
      return contract.onGetExperienceSuccess(value);
    }).catchError((onError){
      return contract.onError(onError);
    });
  }


  @override
  void onError(Exception exception) {
    // TODO: implement onError
    if (Get.isDialogOpen) {
      Get.back();
    }
    Get.snackbar(null, exception.toString());
  }

  @override
  void onUploadAvatarSuccess(String path) {
    // TODO: implement onUploadAvatarSuccess
    pathAvatar = path;
    update();
    onUpdateRequest();
    onUpdateProfile();
  }

  @override
  void onUpdateProfileSuccess(String message) {
    // TODO: implement onUpdateProfileSuccess
    if (Get.isDialogOpen) {
      Get.back();
    }
    Get.snackbar(null, message);
  }

  @override
  void onGetInfoUserSuccess(DetailUserInfo response) {
    // TODO: implement onGetInfoUserSuccess
    emailController.text = response.result.email;
    mobileController.text = response.result.mobile;
    addressController.text = response.result.address;
    if(response.result.postId != null && response.result.postId.isNotEmpty){
      StringBuffer stringBuffer = StringBuffer();
      for(int i = 0; i < response.result.postId.length; i++){
        stringBuffer.write(response.result.postId[i].name);
        if(i != response.result.postId.length - 1){
          stringBuffer.write(", ");
        }
        userPosition = stringBuffer.toString();
      }
    }
    mInfoUser = response;
    update();
  }

  @override
  void onGetExperienceSuccess(ExperienceResponse response) {
    // TODO: implement onGetExperienceSuccess
    mExperience = response.experience;
    mDegree = response.degree;
    update();
  }


}