import 'package:be_staff/data/response/detail_user_info.dart';
import 'package:be_staff/data/response/experience_response.dart';

abstract class ProfileContract {
  void onUploadAvatarSuccess(String path);

  void onUpdateProfileSuccess(String message);

  void onGetInfoUserSuccess(DetailUserInfo response);

  void onGetExperienceSuccess(ExperienceResponse response);

  void onError(Exception exception);
}
