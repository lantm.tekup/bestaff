import 'package:be_staff/ui/upload_avatar/view/upload_avatar_page.dart';
import 'package:be_staff/ui/user_manual/controller/user_manual_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/fonts.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class UserManualPage extends GetView<UserManualController> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    // TODO: implement build
    return GetBuilder<UserManualController>(
        builder: (value) => InitialWidget(
            body: Directionality(
              textDirection: TextDirection.rtl,
              child: SingleChildScrollView(
                reverse: true,
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: screenHeight / 1.3,
                      width: screenWidth,
                      child: PageView(
                        physics: BouncingScrollPhysics(),
                        onPageChanged: (value) {
                          controller.onChangePage(value);
                        },
                        controller: controller.pageController,
                        children: [
                          _buildStepNext(background_intro_step3, title_intro_3,
                              content_intro_3, screenWidth, screenHeight),
                          _buildStepNext(background_intro_step2, title_intro_2,
                              content_intro_2, screenWidth, screenHeight),
                          _buildStepNext(background_intro_step1, title_intro_1,
                              content_intro_1, screenWidth, screenHeight),
                          _buildStepFirst(background_intro_step3, congratulations_first_login,
                              content_intro_3, screenWidth, screenHeight)
                        ],
                      ),
                    ),
                    Container(
                      child: SmoothPageIndicator(
                        controller: controller.pageController,
                        count: 4,
                        effect: WormEffect(
                            dotWidth: 10,
                            dotHeight: 10,
                            activeDotColor: blueMainColorApp),
                      ),
                    ),
                    controller.currentPage != 0 ?
                    Container(
                        margin: EdgeInsets.only(
                            top: 40, left: 20, right: 20, bottom: 20),
                        child: ButtonCustomized(
                          toUpperCase: false,
                          text: next_text,
                          font: fBarlow,
                          weight: FontWeight.w600,
                          backgroundColor: blueMainColorApp,
                          width: screenWidth,
                          height: screenHeight / 16,
                          borderRadius: 240,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          onPressed: () {
                            controller.goBack();
                          },
                        )):Container(margin: EdgeInsets.only(
                        top: 40, left: 20, right: 20, bottom: 20),
                        child: ButtonCustomized(
                          toUpperCase: false,
                          text: understood,
                          font: fBarlow,
                          weight: FontWeight.w600,
                          backgroundColor: blueMainColorApp,
                          width: screenWidth,
                          height: screenHeight / 16,
                          borderRadius: 240,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          onPressed: () {
                            Get.to(UploadAvatarPage());
                          },
                        ))
                  ],
                ),
              ),
            )));
  }
  Widget _buildStepFirst(String imageBackground, String title, String description,
      double screenWidth, double screenHeight) => Container(
    child: Column(
      children: [
        Container(
          width: screenWidth,
          height: screenHeight /1.4,
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextCustomized(
                  text: title,
                  font: fRobotoRegular,
                  size: screenWidth * 0.08,
                  weight: FontWeight.w600,
                  color: black,
                  isCenter: true),
              SizedBox(
                height: 10,
              ),
              TextCustomized(
                  text: description,
                  font: fBarlow,
                  size: screenWidth * 0.03,
                  weight: FontWeight.w400,
                  isCenter: true)
            ],
          ),
        )
      ],
    ),
  );

  Widget _buildStepNext(String imageBackground, String title, String description,
      double screenWidth, double screenHeight) =>
      Container(
        child: Container(
          width: screenWidth,
          height: screenHeight,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 20,
              ),
              TextCustomized(
                  text: quick_use_instructions,
                  font: fBarlow,
                  size: screenWidth * 0.04,
                  weight: FontWeight.w700,
                  isCenter: true),
              SizedBox(
                height: 40,
              ),
              Container(
                  width: screenWidth / 1.3,
                  height: screenHeight / 2.3,
                  child: ImageCustomized(
                    path: imageBackground,
                  )),
              SizedBox(
                height: 18,
              ),
              Container(
                width: screenWidth / 1.1,
                height: screenHeight / 10,
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextCustomized(
                        text: title,
                        font: fBarlowMedium,
                        size: screenWidth * 0.03,
                        weight: FontWeight.w700,
                        isCenter: true),
                    SizedBox(
                      height: 10,
                    ),
                    TextCustomized(
                        text: description,
                        font: fBarlow,
                        size: screenWidth * 0.03,
                        weight: FontWeight.w400,
                        isCenter: true)
                  ],
                ),
              )
            ],
          ),
        ),
      );
}
