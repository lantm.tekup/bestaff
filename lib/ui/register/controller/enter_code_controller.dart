import 'dart:async';
import 'dart:convert';

import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/login_repositories.dart';
import 'package:be_staff/data/repositories/register_repositories.dart';
import 'package:be_staff/data/request/login_request.dart';
import 'package:be_staff/data/response/user_response.dart';
import 'package:be_staff/ui/home/view/main_home_page.dart';
import 'package:be_staff/ui/register/contract/enter_code_contract.dart';
import 'package:be_staff/ui/register/view/tutorial_page.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EnterCodeController extends GetxController implements EnterCodeContract{
  final TextEditingController codeController = TextEditingController();
  RegisterRepositories repositories;
  bool isCodeInvalid = false;
  bool isFieldNull = false;
  String errorText;
  Timer timer;
  LoginRepositories loginRepositories;
  Map arguments;
  EnterCodeContract contract;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    repositories = Injector().onPostRegister;
    contract = this;
    onGetArguments();
  }

  void onSendRequestCode() {
    onCheckCodeValid();
    if (!isFieldNull ) {
      Get.dialog(LoadingDialog());
      repositories.onEnterCode(codeController.text).then((value) {
        return contract.onEnterSuccess();
      }).catchError((onError) {
        return contract.onEnterError(onError);
      });
    }
  }

  void onGetArguments(){
    arguments = Get.arguments;
  }

  void onLogin() {
    loginRepositories = Injector().onLogin;
    LoginRequest request = LoginRequest();
    request.email = arguments['email'];
    request.password = arguments['password'];
    loginRepositories.onLogin(request).then((value) async {
      return contract.onLoginSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }

  void onCheckCodeValid(){
    if(codeController.text.isNotEmpty){
      isFieldNull = false;
    }else{
      isFieldNull = true;
      errorText = this_field_required;
    }
    update();
  }

  void onCancel(){
    Get.to(MainHomePage());
  }

  @override
  void onEnterSuccess() {
    // TODO: implement onEnterSuccess
    onLogin();
  }

  @override
  void onError(Exception exception) {
    // TODO: implement onError
    Get.snackbar(null, exception.toString());
  }

  @override
  Future<void> onLoginSuccess(UserResponse response) async {
    // TODO: implement onLoginSuccess
    final bool isTutorial = arguments['isTutorial'];
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("infoUser", jsonEncode(response.toJson()));
    StorageInfoUser().setInfoUser(response);
    if (isTutorial) {
      Get.offAll(TutorialPage());
    } else {
      Get.offAll(MainHomePage());
    }
  }

  @override
  void onEnterError(Exception exception) {
    // TODO: implement onEnterError
    if(Get.isDialogOpen){
      Get.back();
    }
    isCodeInvalid = true;
    update();
    Timer.periodic(Duration(seconds: 3), (timer) {
      if(timer.tick == 3){
        isCodeInvalid = false;
        update();
        timer.cancel();
      }
    });
  }
}
