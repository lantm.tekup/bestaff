import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/register/controller/enter_code_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:be_staff/widgets/original/text_field_customized.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';

class EnterCodePage extends GetView<EnterCodeController> {
  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return GetBuilder<EnterCodeController>(
      builder: (value) => InitialWidget.expand(
        body: Padding(
          padding: const EdgeInsets.all(22.0),
          child: Column(
            children: [
              Expanded(child: Center(child: onRenderCenterContent())),
              onRenderBottomButton()
            ],
          ),
        ),
      ),
    );
  }



  Widget onRenderCenterContent() {
    RenderParagraph renderParagraph = RenderParagraph(
        TextSpan(
            text: invalid_text,
            style: TextStyle(
                color: ColorHelper.parseColor("#FF3838"),
                fontSize: smallSize,
                fontWeight: FontWeight.w600)),
        maxLines: 1,
        textDirection: TextDirection.ltr);
    double textWidth = renderParagraph.getMinIntrinsicWidth(smallSize).ceilToDouble();
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        TextCustomized(
          text: enter_code_to_login,
          size: 24,
          weight: FontWeight.w500,
          color: Colors.black,
        ),
        SizedBox(
          height: 20,
        ),
        TextFieldCustomized(
          controller: controller.codeController,
          borderRadius: 100,
          hintText: enter_code,
          errorText: controller.errorText,
          validator: () => controller.isFieldNull,
          hintStyle: TextStyle(color: bdGrayColor, fontSize: mediumSize),
          colorBorderEnable: ColorHelper.parseColor("E8E8E8"),
          backgroundColor: ColorHelper.parseColor("F6F6F6"),
          textStyle: TextStyle(
              color: Colors.black,
              fontSize: mediumSize,
              fontWeight: FontWeight.w600),
          suffixIcon: Padding(
            padding: const EdgeInsets.all(16.0),
            child: controller.isCodeInvalid
                ? TextCustomized(
                    text: invalid_text,
                    color: ColorHelper.parseColor("#FF3838"),
                    size: smallSize,
                    weight: FontWeight.w600,
                  )
                : InkWell(
                    onTap: () => controller.onSendRequestCode(),
                    child: ImageCustomized(
                      path: ic_enter_code,
                      width: 22,
                      height: 22,
                    )),
          ),
          suffixIconConstraints: BoxConstraints(minWidth: textWidth),
        ),
        SizedBox(
          height: 20,
        ),
        TextCustomized(
          text: enter_code_to_login_note,
          color: ColorHelper.parseColor("#444444"),
          isCenter: true,
        ),
      ],
    );
  }

  Widget onRenderBottomButton() => Column(
    children: [
      ButtonCustomized(
        text: send_request,
        toUpperCase: false,
        backgroundColor: blueMainColorApp,
        borderRadius: 100,
        width: double.infinity,
        onPressed: () => controller.onSendRequestCode(),
        margin: EdgeInsets.symmetric(horizontal: Get.width/6),
      ),
      SizedBox(height: 16,),
      InkWell(
        onTap: () => controller.onCancel(),
        child: TextCustomized(
          text: cancel_text,
          weight: FontWeight.w600,
          size: mediumSize,
          color: blueMainColorApp,
        ),
      )
    ],
  );
}
