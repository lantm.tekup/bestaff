import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermPrivacyPage extends GetView {
  final String url;

  TermPrivacyPage(this.url);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InitialWidget(body: WebView(
      initialUrl: url,
    ));
  }

}