import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/models/user.dart';
import 'package:be_staff/data/repositories/intro_repositories.dart';
import 'package:be_staff/ui/intro/contract/intro_contract.dart';
import 'package:be_staff/ui/login/view/login_page.dart';
import 'package:be_staff/ui/register/view/register_page.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:be_staff/helper/extension.dart';

class IntroController extends GetxController implements IntroContract {
  int currentPage = 0;
  PageController pageController;
  bool isNextStep = false;
  bool isFocusTextField = false;
  Duration pageTurnDuration = Duration(milliseconds: 500);
  Curve pageTurnCurve = Curves.ease;

  TextEditingController emailController;

  IntroRepositories introRepositories;
  IntroContract contract;

  @override
  void onInit() {
    super.onInit();
    pageController =
        PageController(viewportFraction: 1, initialPage: currentPage);
    introRepositories = Injector().onCheckAccount;
    contract = this;
    emailController = TextEditingController();
  }

  void goForward() {
    pageController.nextPage(duration: pageTurnDuration, curve: pageTurnCurve);
    update();
  }

  void goBack() {
    pageController.previousPage(
        duration: pageTurnDuration, curve: pageTurnCurve);
    update();
  }

  void onChangePage(int page) {
    currentPage = page;
    update();
  }

  void phoneNumberValidator(String value) {
    if (!value.checkPhoneNumber())
      isNextStep = !value.checkPhoneNumber();
    else
      isNextStep = !value.checkEmail();
    update();
  }

  void focusTextField(bool value) {
    isFocusTextField = value;
    update();
  }

  void onCheckAccount() {
    Get.dialog(LoadingDialog());
    introRepositories
        .onCheckAccount(emailController.text)
        .then((value) => contract.onCheckAccountSuccess())
        .catchError((onError) {
          return contract.onCheckAccountError(onError);
    });
  }

  @override
  void onCheckAccountError(Exception exception) {
    Get.back();
    Get.to(RegisterPage(), arguments: emailController.text);
  }

  @override
  void onCheckAccountSuccess() {
    Get.back();
    Get.to(LoginPage(), arguments: emailController.text);
  }
}
