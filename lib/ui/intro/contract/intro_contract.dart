
abstract class IntroContract {
  void onCheckAccountSuccess();
  void onCheckAccountError(Exception exception);
}