import 'package:be_staff/ui/intro/controller/intro_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/fonts.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/values/styles.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:be_staff/widgets/original/text_field_customized.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class IntroPage extends GetView<IntroController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<IntroController>(
        builder: (value) => InitialWidget.expand(
                body: Column(
                  children: <Widget>[
                    SizedBox(height: 16,),
                    ImageCustomized(path: ic_logo_app, width: Get.width, height: 45,),
                    SizedBox(height: 50,),
                    _buildContentIntro(),
                    Container(
                      child: SmoothPageIndicator(
                        controller: controller.pageController,
                        count: 3,
                        effect: WormEffect(
                            dotWidth: 10,
                            dotHeight: 10,
                            activeDotColor: blueMainColorApp),
                      ),
                    ),
                    // controller.currentPage != 2
                    //     ? Container(
                    //         margin: EdgeInsets.only(
                    //             top: 60, left: 20, right: 20, bottom: 20),
                    //         child: ButtonCustomized(
                    //           toUpperCase: false,
                    //           text: next_text,
                    //           font: fBarlow,
                    //           weight: FontWeight.w600,
                    //           backgroundColor: blueMainColorApp,
                    //           borderRadius: 240,
                    //           width: Get.width,
                    //           padding: EdgeInsets.symmetric(vertical: 16),
                    //           onPressed: () {
                    //             controller.goForward();
                    //           },
                    //         ))
                    //     : Container(),
                    _buildBottomButton()
                  ],
                )));
  }

  Widget _buildContentIntro() => Expanded(
    child: SizedBox(
      height: 500,
      child: PageView(
        physics: BouncingScrollPhysics(),
        onPageChanged: (value) {
          controller.onChangePage(value);
        },
        controller: controller.pageController,
        children: [
          _IntroView(background_intro_step1, title_intro_1,
              content_intro_1),
          _IntroView(background_intro_step2, title_intro_2,
              content_intro_2),
          _IntroView(background_intro_step3, title_intro_3,
              content_intro_3),
        ],
      ),
    ),
  );

  Widget _buildBottomButton() => Column(children: [
    Container(
        margin: EdgeInsets.only(
            top: 20, left: 20, right: 20),
        child: TextFieldCustomized(
          onChanged: (value) {
            controller.phoneNumberValidator(value);
          },
          onFocused: (value) {
            controller.focusTextField(value);
          },
          controller: controller.emailController,
          backgroundColor: f6GrayColor,
          borderRadius: 50,
          hintText: enter_email_phone,
          hintStyle: styleHintInputEmailMobile,
          colorBorderDisable: efGrayColor,
          colorBorderEnable: f6GrayColor,
          colorSuffixFocus: efGrayColor,
          textAlign: TextAlign.center,
        )),
    Container(
        margin: EdgeInsets.only(
            top: 15, left: 20, right: 20, bottom: 20),
        child: ButtonCustomized(
          toUpperCase: false,
          text: next_text,
          colorText: controller.isNextStep
              ? white
              : bdGrayColor,
          font: fBarlow,
          weight: FontWeight.w600,
          backgroundColor: controller.isNextStep
              ? blueMainColorApp
              : f6GrayColor,
          width: Get.width,
          borderRadius: 240,
          padding: EdgeInsets.symmetric(vertical: 16),
          onPressed: controller.isNextStep
              ? () {
            controller.onCheckAccount();
          }
              : () {},
        ))
  ]);
}

class _IntroView extends GetView {
  final String imageBackground;
  final String title;
  final String description;

  _IntroView(this.imageBackground, this.title, this.description);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ImageCustomized(
          path: imageBackground,
          height: 300,
          width: Get.width,
        ),
        SizedBox(
          height: 18,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextCustomized(
                  text: title,
                  font: fBarlowMedium,
                  weight: FontWeight.w700,
                  isCenter: true),
              SizedBox(
                height: 10,
              ),
              TextCustomized(
                  text: description,
                  font: fBarlow,
                  weight: FontWeight.w400,
                  isCenter: true)
            ],
          ),
        )
      ],
    );
  }

}
