import 'package:be_staff/data/response/lack_shift_user_response.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LackOfPeopleOfShiftView extends GetView {
  final List<LackShiftUserHome> lackShiftUser;
  LackOfPeopleOfShiftView(this.lackShiftUser);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextCustomized(
                text: case_miss_person,
                weight: FontWeight.w600,
                size: mediumSize,
              ),
              // TextCustomized(
              //   text: see_all_text + " (${lackShiftUser.length})",
              //   size: smallSize,
              // ),

            ],
          ),
        ),
        lackShiftUser.isNotEmpty ? Container(
          height: 150,
          padding: EdgeInsets.only(left: 24),
          child: ListView.builder(
            itemCount: lackShiftUser.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => Row(
              children: [
                Container(
                  width: Get.width * 2 / 3,
                  margin: EdgeInsets.symmetric(vertical: 8),
                  decoration: BoxDecoration(
                      color: ColorHelper.parseColor("#FBFBFB"),
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 2,
                            offset: Offset(4, 4)
                        )
                      ]),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 8),
                          decoration: BoxDecoration(
                              color: ColorHelper.parseColor("#FF6B6B"),
                              borderRadius: BorderRadius.circular(8)
                          ),
                          child: IntrinsicWidth(child: lackShiftUser[index].getTimes != null ? itemToday(index) : itemNotToday(index)),
                        ),
                      ),
                      SizedBox(width: 7,),
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Row(
                              children: [
                                ImageCustomized(path: ic_clock, width: 8, height: 8,),
                                SizedBox(width: 5,),
                                TextCustomized(
                                  text: lackShiftUser[index].dataItem.startTime + " - " + lackShiftUser[index].dataItem.endTime,
                                ),
                                SizedBox(width: 5,),
                                ImageCustomized(path: ic_rain, width: 17, height: 17,)
                              ],
                            ),
                            Row(
                              children: [
                                ImageCustomized(path: ic_place, width: 8, height: 8,),
                                SizedBox(width: 5,),
                                Expanded(
                                  child: TextCustomized(
                                    text: lackShiftUser[index].dataItem.dataPosition[0].position,
                                    textOverflow: TextOverflow.ellipsis,
                                    maxLine: 1,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 8,
                                  height: 8,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                            color: lackShiftUser[index]
                                                        .dataItem
                                                        .dataPosition[0]
                                                        .color !=
                                                    null
                                                ? ColorHelper.parseColor(
                                                    lackShiftUser[index]
                                                        .dataItem
                                                        .dataPosition[0]
                                                        .color)
                                                : null),
                                ),
                                SizedBox(width: 5,),
                                TextCustomized(
                                  text: lackShiftUser[index].dataItem.dataPosition[0].namePosition,
                                  color: lackShiftUser[index]
                                      .dataItem
                                      .dataPosition[0]
                                      .color !=
                                      null
                                      ? ColorHelper.parseColor(
                                      lackShiftUser[index]
                                          .dataItem
                                          .dataPosition[0]
                                          .color)
                                      : null,
                                  size: normalSize,
                                )
                              ],
                            ),
                            // Row(
                            //   crossAxisAlignment: CrossAxisAlignment.start,
                            //   children: [
                            //     CircleImage(path: ic_avatar_example, insideRadius: 12),
                            //     SizedBox(width: 5,),
                            //     CircleImage(path: ic_avatar_example, insideRadius: 12,),
                            //     SizedBox(width: 5,),
                            //     CircleImage(path: ic_avatar_example, insideRadius: 12,),
                            //     SizedBox(width: 5,),
                            //     CircleImage(path: ic_avatar_example, insideRadius: 12,),
                            //   ],
                            // ),

                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: ImageCustomized(path: ic_forward, width: 5, height: 14),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  width: 25,
                )
              ],
            ),
          ),
        ) : Container(child: TextCustomized(text: no_lack_of_people_shift, isCenter: true,),)
      ],
    );
  }

  Widget itemToday(int index) => Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      TextCustomized(
        text: lackShiftUser[index].getTimes.toUpperCase(),
        size: mediumSize,
        color: Colors.white,
      ),
      Container(
        height: 1,
        margin: EdgeInsets.symmetric(horizontal: 20),
        color: Colors.white,
        width: double.infinity,
      ),
      TextCustomized(
        text: lackShiftUser[index].nameDay.toUpperCase(),
        size: 25,
        color: Colors.white,
        isCenter: true,
      ),
      TextCustomized(
        text: lackShiftUser[index].getDate,
        size: 25,
        color: Colors.white,
      ),
    ],
  );

  Widget itemNotToday(int index) => Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      TextCustomized(
        text: lackShiftUser[index].nameDay,
        size: 24,
        isCenter: true,
        color: Colors.white,
      ),
      Container(
        height: 1,
        margin: EdgeInsets.symmetric(horizontal: 20),
        color: Colors.white,
        width: double.infinity,
      ),
      TextCustomized(
        text: lackShiftUser[index].getDate,
        size: 30,
        color: Colors.white,
      ),
    ],
  );

}