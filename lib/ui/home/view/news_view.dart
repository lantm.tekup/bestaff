import 'package:be_staff/data/response/news_response.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/home/view/detail_news_page.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsView extends GetView {
  final NewsResponse news;

  NewsView(this.news);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextCustomized(
                text: news_feed_text,
                weight: FontWeight.w600,
                size: mediumSize,
              ),
              // TextCustomized(
              //   text: see_all_text + " (${news.totalPage})",
              //   size: smallSize,
              // )
            ],
          ),
        ),
        news.news.isNotEmpty
            ? Container(
                height: 150,
                padding: EdgeInsets.only(left: 24),
                child: ListView.builder(
                  itemCount: news?.news?.length ?? 0,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) => Row(
                    children: [
                      InkWell(
                        onTap: () => Get.to(DetailNewsPage(news.news[index])),
                        child: Container(
                          width: Get.width * 2 / 3,
                          margin: EdgeInsets.symmetric(vertical: 8),
                          decoration: BoxDecoration(
                              color: ColorHelper.parseColor("#FBFBFB"),
                              borderRadius: BorderRadius.circular(8),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black.withOpacity(0.1),
                                    blurRadius: 2,
                                    offset: Offset(4, 4))
                              ]),
                          child: Column(
                            children: [
                              ImageCustomized(
                                path: ic_announced,
                                width: 100,
                                height: 100,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 7),
                                child: RichText(
                                    maxLines: 2,
                                    text: TextSpan(
                                        text: news.news[index].title,
                                        style: TextStyle(color: Colors.black),
                                        children: [
                                          TextSpan(
                                              text: "...Xem thêm",
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () => Get.to(
                                                    DetailNewsPage(
                                                        news.news[index])),
                                              style: TextStyle(
                                                  color: blueMainColorApp))
                                        ])),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 25,
                      )
                    ],
                  ),
                ),
              )
            : TextCustomized(
                text: no_data_of_news,
                isCenter: true,
              )
      ],
    );
  }
}
