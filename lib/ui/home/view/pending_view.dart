import 'package:be_staff/data/response/pending_response.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/home/controller/home_controller.dart';
import 'package:be_staff/ui/request_management/view/request_management_page.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/circle_image.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PendingView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final data = controller.mPending.switchShifts;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextCustomized(
                text: waiting_for_approval,
                weight: FontWeight.w600,
                size: mediumSize,
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 14,
                    backgroundColor: ColorHelper.parseColor("#FFB84E"),
                    child: CircleAvatar(
                        radius: 12,
                        backgroundColor: Colors.white,
                        child: TextCustomized(
                          text: controller.pendingCount.toString(),
                          color: ColorHelper.parseColor("#FFB84E"),
                        )),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  CircleAvatar(
                    radius: 14,
                    backgroundColor: ColorHelper.parseColor("#24CE85"),
                    child: CircleAvatar(
                        radius: 12,
                        backgroundColor: Colors.white,
                        child: TextCustomized(
                          text: controller.successCount.toString(),
                          color: ColorHelper.parseColor("#24CE85"),
                        )),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  CircleAvatar(
                    radius: 14,
                    backgroundColor: ColorHelper.parseColor("#EF4F2B"),
                    child: CircleAvatar(
                        radius: 12,
                        backgroundColor: Colors.white,
                        child: TextCustomized(
                          text: controller.rejectCount.toString(),
                          color: ColorHelper.parseColor("#EF4F2B"),
                        )),
                  ),
                ],
              ),
              InkWell(
                onTap: () => Get.to(RequestManagementPage()),
                child: TextCustomized(
                  text: see_all_text
                  // + " +99" // For next version
                  ,
                  size: smallSize,
                ),
              )
            ],
          ),
        ),
        data.isNotEmpty
            ? Container(
                padding: EdgeInsets.all(8),
                margin: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                decoration: BoxDecoration(
                    color: ColorHelper.parseColor("#FBFBFB"),
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          blurRadius: 2,
                          offset: Offset(4, 4))
                    ]),
                child: ListView.builder(
                  physics: ClampingScrollPhysics(),
                  itemBuilder: (context, index) => _buildItem(data[index]),
                  itemCount: data.length,
                  shrinkWrap: true,
                ),
              )
            : TextCustomized(
                text: no_pending_request,
                isCenter: true,
              )
      ],
    );
  }

  Widget _buildItem(SwitchResponse data) {
    final avatar = data.avatar;

    bool isUrl = avatar != null ? Uri.parse(avatar).isAbsolute : false;
    Widget _itemAvatar;
    if (avatar != null && avatar != "" && isUrl) {
      _itemAvatar = CircleImage.network(
        path: avatar,
        insideRadius: 20 / 2,
      );
    } else {
      final String fullName = data.fullName;

      List<String> stringNames = fullName.split(" ");
      var text;
      if(stringNames.length <= 1){
        text = fullName[0].toUpperCase() + fullName[1];
      }else{
        text = stringNames[stringNames.length - 2][0] +
            stringNames[stringNames.length - 1][0];
      }

      _itemAvatar = CircleAvatar(
        backgroundColor: ColorHelper.parseColor("#E1E1E1"),
        radius: 12,
        child: TextCustomized(
          text: text,
          size: smallSize,
        ),
      );
    }
    return Column(
      children: [
        Column(
          children: [
            Row(
              children: [
                _itemAvatar,
                SizedBox(
                  width: 4,
                ),
                Expanded(
                  child: RichText(
                      text: TextSpan(
                          text: data.fullName + " muốn",
                          style: TextStyle(
                              color: Colors.black, fontSize: normalSize),
                          children: [
                        TextSpan(
                          text: " nhượng ca ",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: normalSize,
                              fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: "cho ${data.switchName}",
                          style: TextStyle(
                              color: Colors.black, fontSize: normalSize),
                        )
                      ])),
                ),
                TextCustomized(
                  text: "8 phút",
                  color: b0GrayColor,
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                SizedBox(
                  width: 24,
                ),
                TextCustomized(
                  text: data.namePosition,
                  color: b0GrayColor,
                  size: smallSize,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  width: 4,
                  height: 4,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: b0GrayColor,
                  ),
                ),
                TextCustomized(
                  text: data.getDate,
                  color: b0GrayColor,
                  size: smallSize,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  width: 4,
                  height: 4,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: b0GrayColor,
                  ),
                ),
                TextCustomized(
                  text: data.getStartToEndTime,
                  color: b0GrayColor,
                  size: smallSize,
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 8,
        )
      ],
    );
  }
}
