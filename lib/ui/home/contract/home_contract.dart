import 'package:be_staff/data/response/lack_shift_user_response.dart';
import 'package:be_staff/data/response/news_response.dart';
import 'package:be_staff/data/response/pending_response.dart';
import 'package:be_staff/data/response/shift_notification_response.dart';
import 'package:be_staff/data/response/shift_response/shift_user_response.dart';

abstract class HomeContract {
  void onGetNewsSuccess(NewsResponse response);

  void onError(Exception exception);

  void onGetShiftUserSuccess(ShiftUserResponse response);

  void onGetLackShiftUserSuccess(LackShiftUserResponse response);

  void onGetPendingSuccess(PendingResponse response);

  void onGetShiftNotificationSuccess(ShiftNotificationResponse response);
}
