abstract class ShiftUserContract {
  void onCheckInSuccess(int response);

  void onSaveTimeKeepingSuccess(int response);

  void onError(Exception exception);
}
