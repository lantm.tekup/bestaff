import 'package:be_staff/data/response/type_of_leave_response.dart';

abstract class TakeLeaveContract {
  void onGetTypeSuccess(TypeOfLeaveResponse response);

  void onAddLeaveSuccess(String message);

  void onGetError(Exception exception);
}
