import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/overtime_repositories.dart';
import 'package:be_staff/data/request/overtime_request.dart';
import 'package:be_staff/data/response/information_overtime_response.dart';
import 'package:be_staff/helper/date_time_helper.dart';
import 'package:be_staff/ui/request_management/contract/overtime_contract.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class OvertimeController extends GetxController implements OvertimeContract {
  bool isShowBottomSheet = false;
  TextEditingController reasonController;
  int countLength = 0;

  OvertimeRepositories repositories;
  OvertimeContract contract;

  String timeIn;
  String timeout;
  String startDateRequest;
  String endDateRequest;

  var mData;
  var mType;
  var mPosition;

  DataPosition dataPosition;
  TypeOvertime typeOvertime;

  bool hasData = false;

  OvertimeRequest mRequest;

  //Validate
  bool hasDataTimeIn = true;
  bool hasDataTimeOut = true;
  bool hasDataPosition = true;
  bool hasDataOvertime = true;
  bool hasDataReason = true;
  bool hasDataDate = true;

  void onChangeReasonController(String value) {
    countLength = value.length;
    update();
  }

  void onUpdateTimeIn(DateTime dateTime) {
    String formatTime = "HH:mm";
    String result = DateFormat(formatTime).format(dateTime);
    timeIn = result;
    onValidate();
  }

  void onUpdateTimeOut(DateTime dateTime) {
    String formatTime = "HH:mm";
    String result = DateFormat(formatTime).format(dateTime);
    timeout = result;
    onValidate();
  }

  void onUpdateDate(DateRangePickerSelectionChangedArgs args) {
    final PickerDateRange pickerDateRange = args.value;
    startDateRequest = DateTimeHelper.toDayMonthYear(
        pickerDateRange.startDate.toIso8601String());
    endDateRequest = DateTimeHelper.toDayMonthYear(
        pickerDateRange.endDate.toIso8601String());
  }

  void onUpdatePosition(DataPosition data) {
    dataPosition = data;
    onValidate();
  }

  void onUpdateTypeOvertime(TypeOvertime data) {
    typeOvertime = data;
    onValidate();
  }

  void onGetInfoOvertime() =>
      repositories.onGetInformationOvertime().then((value) {
        return contract.onGetInfoSuccess(value);
      }).catchError((onError) {
        return contract.onError(onError);
      });

  void onValidate(){
    if(startDateRequest != null || endDateRequest != null){
      hasDataDate = true;
    }else{
      hasDataDate = false;
      Get.dialog(MessageDialog(
        title: error_text,
        description: pick_date_please,
      ));
      return;
    }
    if(timeIn != null){
      hasDataTimeIn = true;
    }else{
      hasDataTimeIn = false;
    }
    if(timeout != null){
      hasDataTimeOut = true;
    }else{
      hasDataTimeOut = false;
    }
    if(dataPosition != null){
      hasDataPosition = true;
    }else{
      hasDataPosition = false;
    }
    if(typeOvertime != null){
      hasDataOvertime = true;
    }else{
      hasDataOvertime = false;
    }
    if(reasonController.text != ""){
      hasDataReason = true;
    }else{
      hasDataReason = false;
    }
    update();
  }

  void onCreateOvertimeRequest(){
    onValidate();
    if (hasDataReason && hasDataOvertime && hasDataPosition && hasDataTimeIn && hasDataTimeOut && hasDataDate) {
      Get.dialog(LoadingDialog());
      mRequest = OvertimeRequest();
      mRequest.postId = dataPosition.id.toString();
      mRequest.fromDate = startDateRequest;
      mRequest.toDate = endDateRequest ?? startDateRequest;
      mRequest.startHour = timeIn;
      mRequest.endHour = timeout;
      mRequest.typeId = typeOvertime.id.toString();
      mRequest.reason = reasonController.text;
      mRequest.buttonAction = "insert";
      repositories.onCreateOvertimeRequest(mRequest).then((value) {
        return contract.onCreateOvertime(value);
      }).catchError((onError){
        return contract.onError(onError);
      });
    }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    reasonController = TextEditingController();
    repositories = Injector().onGetOvertime;
    contract = this;
    onGetInfoOvertime();
    reasonController.addListener(() {
      if(reasonController.text != ""){
        hasDataReason = true;
      }
    });
  }

  @override
  void onCreateOvertime(String message) {
    // TODO: implement onCreateOvertime
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.snackbar(null, message);
  }

  @override
  void onError(Exception exception) {
    // TODO: implement onError
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.dialog(MessageDialog(
      title: error_text,
      description: exception.toString(),
    ));
  }

  @override
  void onGetInfoSuccess(InformationOvertimeResponse response) {
    mData = response.result;
    mType = response.typeOvertime;
    mPosition = response.dataPosition;
    hasData = true;
    update();
  }
}
