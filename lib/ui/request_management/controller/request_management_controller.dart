import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/i_request_management_repository.dart';
import 'package:be_staff/data/response/shift_notification_response.dart';
import 'package:be_staff/data/response/switch_shift_pending_response.dart';
import 'package:be_staff/ui/request_management/contract/request_management_contract.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RequestManagementController extends GetxController  with SingleGetTickerProviderMixin implements RequestManagementContract{
  TabController tabController;
  int indexTab = 0;
  List<SwitchShiftPending> mSwitchShiftPending;
  ShiftNotificationResponse mShiftNotification;
  List<Output> mPending;
  List<Output> mLeave;
  List<Output> mOvertime;
  List<Output> mAll;
  IRequestManagementRepository repository;
  RequestManagementContract contract;

  //For animation FAB
  AnimationController fabAnimationController;
  Animation<double> animation;
  bool isFabOpen = false;

  String page = "1";
  String limit = "7";

  TextEditingController bottomSheetController;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    // tabController = TabController(length: 4, vsync: this);
    // tabController.addListener(() {
    //   indexTab = tabController.index;
    //   update();
    // });
    repository = Injector().onGetRequestManagement;
    contract = this;
    // onGetSwitchShiftPending();
    onGetShiftNotification();
    bottomSheetController = TextEditingController();

    //For animation FAB
    fabAnimationController = AnimationController(duration: Duration(milliseconds: 300), vsync: this);
  }

  void onFabTranslation(){
    if(!isFabOpen){
      fabAnimationController.forward();
    }else{
      fabAnimationController.reverse();
    }
    isFabOpen = !isFabOpen;
    update();
  }

  void onGetSwitchShiftPending() {
    repository.onGetSwitchPending().then((value) {
      return contract.onGetSwitchPendingShiftSuccess(value.result);
    }).catchError((error){
      return contract.onError(error);
    });
  }

  void onGetShiftNotification() {
    repository.onGetShiftNotification(limit, page).then((value) {
      return contract.onGetShiftNotificationSuccess(value);
    }).catchError((onError){
      return contract.onError(onError);
    });
  }

  void onAccept(int position){
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.dialog(LoadingDialog());
    repository.onAcceptSwitchShift(mSwitchShiftPending[position].id).then((value) {
      return contract.onAcceptSuccess(value);
    }).catchError((error){
      return contract.onError(error);
    });
  }

  void onReject(int position){
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.dialog(LoadingDialog());
    repository.onRejectSwitchShift(mSwitchShiftPending[position].id).then((value) {
      return contract.onRejectSuccess(value);
    }).catchError((error){
      return contract.onError(error);
    });
  }

  Future<void> onRefresh() async{
    onGetShiftNotification();
  }

  @override
  void onAcceptSuccess(String message) {
    // TODO: implement onAcceptSuccess
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.snackbar(null, message);
    onGetSwitchShiftPending();
  }

  @override
  void onError(String error) {
    // TODO: implement onError
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.dialog(MessageDialog(
      description: error,
    ));
  }

  @override
  void onGetSwitchPendingShiftSuccess(List<SwitchShiftPending> response) {
    // TODO: implement onGetSwitchPendingShiftSuccess
    mSwitchShiftPending = response;
    update();
  }

  @override
  void onRejectSuccess(String message) {
    // TODO: implement onRejectSuccess
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.snackbar(null, message);
    onGetSwitchShiftPending();
  }

  @override
  void onGetShiftNotificationSuccess(ShiftNotificationResponse response) {
    // TODO: implement onGetShiftNotificationSuccess
    mShiftNotification = response;
    mPending = response.output;
    mLeave = response.outputLeave;
    mOvertime = response.outputOT;
    mAll = List();
    if (mPending != null) {
      mAll.addAll(mPending);
    }
    if(mLeave != null){
      mAll.addAll(mLeave);
    }
    if(mOvertime != null){
      mAll.addAll(mOvertime);
    }
    update();

  }


}