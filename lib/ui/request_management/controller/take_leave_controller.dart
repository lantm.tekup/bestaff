import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/take_leave_repositories.dart';
import 'package:be_staff/data/request/take_leave_request.dart';
import 'package:be_staff/data/response/type_of_leave_response.dart';
import 'package:be_staff/helper/date_time_helper.dart';
import 'package:be_staff/ui/request_management/controller/take_leave_contract.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class TakeLeaveController extends GetxController implements TakeLeaveContract {
  bool isShowBottomSheet = false;
  int countLength = 0;
  bool hasChange = false;
  TakeLeaveRepositories repositories;
  TakeLeaveContract contract;

  TypeOfLeaveResponse typeOfLeaveResponse;
  String typeId;
  String type;

  bool hasReasonError = false;
  bool hasDateError = false;
  bool hasTypeError = false;
  bool isPickedDate = false;
  String startDate;
  String endDate;

  TextEditingController reasonController;


  bool onIsShowBottomSheet(DateRangePickerSelectionChangedArgs value) {
    PickerDateRange pickerDateRange = value.value;
    if (pickerDateRange.startDate != null && pickerDateRange.endDate != null) {
      return true;
    }
    return false;
  }

  void onChangeReasonController(String value) {
    countLength = value.length;
    update();
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    reasonController = TextEditingController();
    repositories = Injector().onGetTakeLeave;
    contract = this;
    onGetListOfType();
  }

  void onGetListOfType() {
    repositories.onGetTypeOfLeave().then((value) {
      return contract.onGetTypeSuccess(value);
    }).catchError((onError) {
      return contract.onGetError(onError);
    });
  }

  void onHandlingDate(DateRangePickerSelectionChangedArgs dateRangePickerSelectionChangedArgs){
    if (dateRangePickerSelectionChangedArgs != null) {
      hasDateError = false;
      PickerDateRange date = dateRangePickerSelectionChangedArgs.value;

      final _startDateRequest = DateTimeHelper.toDayMonthYear(date.startDate.toIso8601String());
      var _endDateRequest;
      if(date.endDate != null){
        _endDateRequest = DateTimeHelper.toDayMonthYear(date.endDate.toIso8601String());
      }
      startDate = _startDateRequest;
      endDate = _endDateRequest;
      update();
    }
  }

  void onCheckReasonValid(){
    if(reasonController.text == ""){
      hasReasonError = true;
    }else{
      hasReasonError = false;
    }
    if(startDate == null && endDate == null){
      hasDateError = true;
      Get.dialog(MessageDialog(
        title: error_text,
        description: pick_date_please,
      ));
    }
    if(typeId == null){
      hasTypeError = true;
      Get.dialog(MessageDialog(
        title: error_text,
        description: pick_type_of_reason,
      ));
    }
    update();
  }

  void onUpdateType(int position){
    hasTypeError = false;
    type = typeOfLeaveResponse.result[position].name;
    typeId = typeOfLeaveResponse.result[position].id.toString();
    update();
    if(Get.isBottomSheetOpen){
      Get.back();
    }
  }

  void onTakeLeaveRequest() {
    onCheckReasonValid();
    if (!hasReasonError && !hasTypeError && !hasDateError) {
      Get.dialog(LoadingDialog());
      LeaveRequest request = LeaveRequest();
      request.fromDate = startDate;
      request.toDate = endDate ?? startDate;
      request.reason = reasonController.text;
      request.typeId = typeId;
      repositories.onAddLeave(request).then((value) {
        return contract.onAddLeaveSuccess(value);
      }).catchError((onError) {
        return contract.onGetError(onError);
      });
    }
  }

  @override
  void onAddLeaveSuccess(String message) {
    // TODO: implement onAddLeaveSuccess
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.snackbar(null, "Khởi tạo thành công");
  }

  @override
  void onGetError(Exception exception) {
    // TODO: implement onGetError
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.snackbar(null, exception.toString());
  }

  @override
  void onGetTypeSuccess(TypeOfLeaveResponse response) {
    // TODO: implement onGetTypeSuccess
    typeOfLeaveResponse = response;
    update();
    if(Get.isDialogOpen){
      Get.back();
    }
  }
}
