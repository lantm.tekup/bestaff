abstract class LackPeopleContract {
  void onReceiveSuccess(String message);

  void onCancelSuccess(String message);

  void onError(Exception error);
}
