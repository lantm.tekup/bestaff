import 'package:be_staff/data/response/busy_time_response.dart';
import 'package:be_staff/data/response/calendar_response.dart';

abstract class BusyTimeContract {
  void onGetBusyTimeSuccess(BusyTimeResponse response);


  void onError(Exception exception);

  void onUpdateSuccess(String message);

}
