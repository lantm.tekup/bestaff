import 'package:be_staff/data/response/calendar_response.dart';
import 'package:be_staff/data/response/lack_shift_user_response.dart';
import 'package:be_staff/data/response/other_shift_response.dart';
import 'package:be_staff/data/response/shift_response/shift_user_response.dart';

abstract class CalendarContract {
  // void onGetWorkingCalendarSuccess(CalendarResponse response);

  void onError(Exception exception);

  void onGetLackShiftUser(LackShiftUserResponse response);

  void onGetUserShift(ShiftUserResponse response);

  void onGetAllShiftsSuccess(List<OtherShiftResponse> response);
}