import 'dart:io';

import 'package:flutter/material.dart';

class CircleImage extends StatelessWidget {
  final String path;
  final double insideRadius;
  final double outsideRadius;
  final Color insideColor;
  final Color outsideColor;
  final bool isNetwork;
  final File file;

  CircleImage(
      {this.path,
      this.insideRadius,
      this.insideColor,
      this.outsideColor,
      this.outsideRadius})
      : isNetwork = false,
        file = null;

  CircleImage.network(
      {this.path,
      this.insideRadius,
      this.insideColor,
      this.outsideColor,
      this.outsideRadius})
      : isNetwork = true,
        file = null;

  CircleImage.file(
      {this.file,
      this.insideRadius,
      this.insideColor,
      this.outsideColor,
      this.outsideRadius})
      : isNetwork = false,
        path = null;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: outsideRadius ?? insideRadius,
      backgroundColor: outsideColor,
      child: CircleAvatar(
        backgroundColor: insideColor ?? Colors.transparent,
        radius: insideRadius ?? 20,
        backgroundImage: file != null
            ? FileImage(file)
            : isNetwork
                ? NetworkImage(path)
                : AssetImage(path),
      ),
    );
  }
}
