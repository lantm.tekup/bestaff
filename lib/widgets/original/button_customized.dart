import 'package:flutter/material.dart';

class ButtonCustomized extends StatelessWidget {
  final String text;
  final Widget child;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry padding;
  final double borderRadius;
  final String font;
  final Color colorText;
  final double size;
  final FontWeight weight;
  final Color backgroundColor;
  final Function onPressed;
  final double width;
  final double height;
  final Border border;
  final bool toUpperCase;

  ButtonCustomized(
      {this.text,
      this.child,
      this.margin,
      this.padding,
      this.borderRadius,
      this.font,
      this.colorText,
      this.size,
      this.weight,
      this.backgroundColor,
      this.width,
      this.height,
      this.border,
      this.toUpperCase,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    double luminance =
        backgroundColor != null ? backgroundColor.computeLuminance() : 0;
    return GestureDetector(
      onTap: () => onPressed != null ? onPressed() : null,
      child: Container(
          padding: padding ?? EdgeInsets.all(16),
          width: width ?? null,
          height: height ?? null,
          margin: margin,
          decoration: BoxDecoration(
              color: backgroundColor ?? Theme.of(context).buttonColor,
              border: border,
              borderRadius: borderRadius != null
                  ? BorderRadius.circular(borderRadius)
                  : BorderRadius.circular(12)),
          child: child ?? Text(
            toUpperCase == null ? text.toUpperCase() : text,
            style: TextStyle(
                fontSize: size ?? 16,
                fontWeight: weight ?? FontWeight.w600,
                fontFamily: font,
                color: colorText != null
                    ? colorText
                    : luminance > 0.5
                        ? Colors.black
                        : Colors.white),
            textAlign: TextAlign.center,
          )),
    );
  }
}
