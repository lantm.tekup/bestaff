import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateTimeHelper {
  static String toDayMonthYear(String iso8601String) {
    var format = "dd-MM-yyyy";
    if (iso8601String != "") {
      DateTime tempDate = DateTime.parse(iso8601String);
      String result = DateFormat(format).format(tempDate);
      return result;
    } else
      return iso8601String;
  }

  static String toYearMonthDay(String iso8601String) {
    var format = "yyyy-MM-dd";
    if (iso8601String != "") {
      DateTime tempDate = DateTime.parse(iso8601String);
      String result = DateFormat(format).format(tempDate);
      return result;
    } else
      return iso8601String;
  }

  static DateTime toDateTime({String hms, String yMd, String dMy}) {
    DateTime result;
    if (hms != null && yMd != null) {
      var format = "yyyy-MM-dd hh:mm:ss";
      result = new DateFormat(format).parse("$yMd$hms");
    }
    if (hms == null && yMd != null) {
      var format = "yyyy-MM-dd";
      result = DateFormat(format).parse(yMd);
    }

    if (hms == null && dMy != null) {
      var format = "dd-MM-yyyy";
      result = DateFormat(format).parse(dMy);
    }
    return result;
  }

  static String getMonthNameFromNumber(int month) {
    List<String> months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
    return months[month - 1];
  }

  static String getMonthNameFromDateTime(DateTime dateTime) {
    return DateFormat('MMMM').format(dateTime);
  }

  static String getFullDay(DateTime date) {
    return DateFormat('EEEE, MMMM dd, yyyy').format(date);
  }

  static int getDaysInMonth(DateTime dateTime) {
    var lastDayOfMonth = DateTime(dateTime.year, dateTime.month + 1, 0);
    return lastDayOfMonth.day;
  }

  static List<DateTime> calculatorListDate(
      {DateTime startDate, DateTime endDate}) {
    if (startDate != null && endDate != null) {
      final countDays = endDate.difference(startDate).inDays;
      List<DateTime> days = List();
      for (int i = 0; i <= countDays; i++) {
        days.add(
            DateTime(startDate.year, startDate.month, startDate.day + (i)));
      }
      return days;
    }
    return null;
  }

  static double timeOfDayToDouble(TimeOfDay timeOfDay) => timeOfDay.hour + timeOfDay.minute / 60;

  static TimeOfDay toTimeOfDayFromString(String time){
    String format = "HH:mm:ss";
    TimeOfDay timeOfDay = TimeOfDay.fromDateTime(DateFormat(format).parse(time));
    return timeOfDay;
  }

  static String toStringFromTimeOfDay(TimeOfDay timeOfDay) => timeOfDay.hour.toString() + ":" + timeOfDay.minute.toString();

}
