class ExperienceResponse {
  List<Experience> degree;
  List<Experience> experience;

  ExperienceResponse({this.degree, this.experience});

  ExperienceResponse.fromJson(Map<String, dynamic> json) {
    if (json['degree'] != null) {
      degree = new List<Experience>();
      json['degree'].forEach((v) {
        degree.add(new Experience.fromJson(v));
      });
    }
    if (json['experience'] != null) {
      experience = new List<Experience>();
      json['experience'].forEach((v) {
        experience.add(new Experience.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.degree != null) {
      data['degree'] = this.degree.map((v) => v.toJson()).toList();
    }
    if (this.experience != null) {
      data['experience'] = this.experience.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Experience {
  int id;
  int branchId;
  int userId;
  int typeId;
  String name;
  String key;
  String dateTime;
  int createBy;
  Null updateBy;
  Null createdAt;
  Null updatedAt;

  Experience(
      {this.id,
        this.branchId,
        this.userId,
        this.typeId,
        this.name,
        this.key,
        this.dateTime,
        this.createBy,
        this.updateBy,
        this.createdAt,
        this.updatedAt});

  Experience.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    branchId = json['branch_id'];
    userId = json['user_id'];
    typeId = json['type_id'];
    name = json['name'];
    key = json['key'];
    dateTime = json['date_time'];
    createBy = json['create_by'];
    updateBy = json['update_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['branch_id'] = this.branchId;
    data['user_id'] = this.userId;
    data['type_id'] = this.typeId;
    data['name'] = this.name;
    data['key'] = this.key;
    data['date_time'] = this.dateTime;
    data['create_by'] = this.createBy;
    data['update_by'] = this.updateBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
