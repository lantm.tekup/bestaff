class DetailUserInfo {
  Result result;

  DetailUserInfo({this.result});

  DetailUserInfo.fromJson(Map<String, dynamic> json) {
    result =
    json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class Result {
  int id;
  int companyId;
  String fullName;
  String code;
  String email;
  String deptId;
  List<PostId> postId;
  int branchId;
  int managerAssign;
  int isManager;
  int ceo;
  String token;
  String mobile;
  Null changedPasswordAt;
  int locked;
  int language;
  String lastVisitedAt;
  int status;
  String birthday;
  int sex;
  Null description;
  String avatar;
  int typeUser;
  String testDateAt;
  String joinDateAt;
  String salary;
  int typeSalary;
  int probationarySalary;
  int expYear;
  String cmndFace;
  String cmndBack;
  int directorPosition;
  String content;
  String dateOff;
  int checkOnline;
  String facebook;
  int pin;
  String deviceToken;
  int persionalId;
  String allowance;
  int insurance;
  int checkTimekeeper;
  int checkInfo;
  int checkLock;
  int createBy;
  int updateBy;
  String createdAt;
  String updatedAt;
  String codeCompany;
  int firstLogin;
  String address;
  Null informationCustomer;
  String branchName;
  String location;

  Result(
      {this.id,
        this.companyId,
        this.fullName,
        this.code,
        this.email,
        this.deptId,
        this.postId,
        this.branchId,
        this.managerAssign,
        this.isManager,
        this.ceo,
        this.token,
        this.mobile,
        this.changedPasswordAt,
        this.locked,
        this.language,
        this.lastVisitedAt,
        this.status,
        this.birthday,
        this.sex,
        this.description,
        this.avatar,
        this.typeUser,
        this.testDateAt,
        this.joinDateAt,
        this.salary,
        this.typeSalary,
        this.probationarySalary,
        this.expYear,
        this.cmndFace,
        this.cmndBack,
        this.directorPosition,
        this.content,
        this.dateOff,
        this.checkOnline,
        this.facebook,
        this.pin,
        this.deviceToken,
        this.persionalId,
        this.allowance,
        this.insurance,
        this.checkTimekeeper,
        this.checkInfo,
        this.checkLock,
        this.createBy,
        this.updateBy,
        this.createdAt,
        this.updatedAt,
        this.codeCompany,
        this.firstLogin,
        this.address,
        this.informationCustomer,
        this.branchName,
        this.location});

  Result.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyId = json['company_id'];
    fullName = json['full_name'];
    code = json['code'];
    email = json['email'];
    deptId = json['dept_id'];
    if (json['post_id'] != null) {
      postId = new List<PostId>();
      json['post_id'].forEach((v) {
        postId.add(new PostId.fromJson(v));
      });
    }
    branchId = json['branch_id'];
    managerAssign = json['manager_assign'];
    isManager = json['is_manager'];
    ceo = json['ceo'];
    token = json['token'];
    mobile = json['mobile'];
    changedPasswordAt = json['changed_password_at'];
    locked = json['locked'];
    language = json['language'];
    lastVisitedAt = json['last_visited_at'];
    status = json['status'];
    birthday = json['birthday'];
    sex = json['sex'];
    description = json['description'];
    avatar = json['avatar'];
    typeUser = json['type_user'];
    testDateAt = json['test_date_at'];
    joinDateAt = json['join_date_at'];
    salary = json['salary'];
    typeSalary = json['type_salary'];
    probationarySalary = json['probationary_salary'];
    expYear = json['exp_year'];
    cmndFace = json['cmnd_face'];
    cmndBack = json['cmnd_back'];
    directorPosition = json['director_position'];
    content = json['content'];
    dateOff = json['date_off'];
    checkOnline = json['check_online'];
    facebook = json['facebook'];
    pin = json['pin'];
    deviceToken = json['device_token'];
    persionalId = json['persional_id'];
    allowance = json['allowance'];
    insurance = json['insurance'];
    checkTimekeeper = json['check_timekeeper'];
    checkInfo = json['check_info'];
    checkLock = json['check_lock'];
    createBy = json['create_by'];
    updateBy = json['update_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    codeCompany = json['code_company'];
    firstLogin = json['first_login'];
    address = json['address'];
    informationCustomer = json['information_customer'];
    branchName = json['branch_name'];
    location = json['location'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['company_id'] = this.companyId;
    data['full_name'] = this.fullName;
    data['code'] = this.code;
    data['email'] = this.email;
    data['dept_id'] = this.deptId;
    if (this.postId != null) {
      data['post_id'] = this.postId.map((v) => v.toJson()).toList();
    }
    data['branch_id'] = this.branchId;
    data['manager_assign'] = this.managerAssign;
    data['is_manager'] = this.isManager;
    data['ceo'] = this.ceo;
    data['token'] = this.token;
    data['mobile'] = this.mobile;
    data['changed_password_at'] = this.changedPasswordAt;
    data['locked'] = this.locked;
    data['language'] = this.language;
    data['last_visited_at'] = this.lastVisitedAt;
    data['status'] = this.status;
    data['birthday'] = this.birthday;
    data['sex'] = this.sex;
    data['description'] = this.description;
    data['avatar'] = this.avatar;
    data['type_user'] = this.typeUser;
    data['test_date_at'] = this.testDateAt;
    data['join_date_at'] = this.joinDateAt;
    data['salary'] = this.salary;
    data['type_salary'] = this.typeSalary;
    data['probationary_salary'] = this.probationarySalary;
    data['exp_year'] = this.expYear;
    data['cmnd_face'] = this.cmndFace;
    data['cmnd_back'] = this.cmndBack;
    data['director_position'] = this.directorPosition;
    data['content'] = this.content;
    data['date_off'] = this.dateOff;
    data['check_online'] = this.checkOnline;
    data['facebook'] = this.facebook;
    data['pin'] = this.pin;
    data['device_token'] = this.deviceToken;
    data['persional_id'] = this.persionalId;
    data['allowance'] = this.allowance;
    data['insurance'] = this.insurance;
    data['check_timekeeper'] = this.checkTimekeeper;
    data['check_info'] = this.checkInfo;
    data['check_lock'] = this.checkLock;
    data['create_by'] = this.createBy;
    data['update_by'] = this.updateBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['code_company'] = this.codeCompany;
    data['first_login'] = this.firstLogin;
    data['address'] = this.address;
    data['information_customer'] = this.informationCustomer;
    data['branch_name'] = this.branchName;
    data['location'] = this.location;
    return data;
  }
}

class PostId {
  int id;
  String name;

  PostId({this.id, this.name});

  PostId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
