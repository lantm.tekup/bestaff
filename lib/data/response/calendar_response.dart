import 'package:intl/intl.dart';

class CalendarResponse {
  final List<WorkingCalendar> t2;
  final List<WorkingCalendar> t3;
  final List<WorkingCalendar> t4;
  final List<WorkingCalendar> t5;
  final List<WorkingCalendar> t6;
  final List<WorkingCalendar> t7;
  final List<WorkingCalendar> cn;

  CalendarResponse(
      {this.t2, this.t3, this.t4, this.t5, this.t6, this.t7, this.cn});

  CalendarResponse.fromJson(Map<String, dynamic> map)
      : t2 = (map['T2'] as List).map((e) => WorkingCalendar.fromJson(e)).toList(),
        t3 = (map['T3'] as List).map((e) => WorkingCalendar.fromJson(e)).toList(),
        t4 = (map['T4'] as List).map((e) => WorkingCalendar.fromJson(e)).toList(),
        t5 = (map['T5'] as List).map((e) => WorkingCalendar.fromJson(e)).toList(),
        t6 = (map['T6'] as List).map((e) => WorkingCalendar.fromJson(e)).toList(),
        t7 = (map['T7'] as List).map((e) => WorkingCalendar.fromJson(e)).toList(),
        cn = (map['CN'] as List).map((e) => WorkingCalendar.fromJson(e)).toList();
}

class WorkingCalendar {
  String startTime;
  String endTime;
  int shiftBranchId;
  ShiftBranch shiftBranch;

  WorkingCalendar(
      {this.startTime, this.endTime, this.shiftBranchId, this.shiftBranch});

  WorkingCalendar.fromJson(Map<String, dynamic> json) {
    startTime = time(json['start_time']);
    endTime = time(json['end_time']);
    shiftBranchId = json['shiftbranch_id'];
    shiftBranch = json['shift_branch'] != null
        ? new ShiftBranch.fromJson(json['shift_branch'])
        : null;
  }
  String time(String input) {
    String format = "hh:mm:ss";
    DateTime dateTime = DateFormat(format).parse(input);
    String result = DateFormat("hh:mm").format(dateTime);
    return result;
  }
}

class ShiftBranch {
  int id;
  int branchId;
  int day;
  int createBy;
  int updateBy;
  String createdAt;
  String updatedAt;

  ShiftBranch(
      {this.id,
      this.branchId,
      this.day,
      this.createBy,
      this.updateBy,
      this.createdAt,
      this.updatedAt});

  ShiftBranch.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    branchId = json['branch_id'];
    day = json['day'];
    createBy = json['create_by'];
    updateBy = json['update_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['branch_id'] = this.branchId;
    data['day'] = this.day;
    data['create_by'] = this.createBy;
    data['update_by'] = this.updateBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
