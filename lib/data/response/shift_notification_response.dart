class ShiftNotificationResponse {
  List<Output> output;
  List<Output> outputLeave;
  List<Output> outputOT;
  CountTotal countTotal;

  ShiftNotificationResponse(
      {this.output, this.outputLeave, this.outputOT, this.countTotal});

  ShiftNotificationResponse.fromJson(Map<String, dynamic> json) {
    if (json['output'] != null) {
      output = new List<Output>();
      json['output'].forEach((v) {
        output.add(new Output.fromJson(v));
      });
    }
    outputLeave = json['outputLeave'];
    outputOT = json['outputOT'];
    countTotal = json['CountTotal'] != null
        ? new CountTotal.fromJson(json['CountTotal'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.output != null) {
      data['output'] = this.output.map((v) => v.toJson()).toList();
    }
    data['outputLeave'] = this.outputLeave;
    data['outputOT'] = this.outputOT;
    if (this.countTotal != null) {
      data['CountTotal'] = this.countTotal.toJson();
    }
    return data;
  }
}

class Output {
  int id;
  int branchId;
  int userId;
  int shiftDetailuserId;
  String content;
  int createBy;
  Null updateBy;
  String createdAt;
  String updatedAt;
  int status;
  String fullNameReceiver;
  String avatarReceiver;
  String fullName;
  String avatar;
  int isMe;

  Output(
      {this.id,
        this.branchId,
        this.userId,
        this.shiftDetailuserId,
        this.content,
        this.createBy,
        this.updateBy,
        this.createdAt,
        this.updatedAt,
        this.status,
        this.fullNameReceiver,
        this.avatarReceiver,
        this.fullName,
        this.avatar,
        this.isMe});

  Output.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    branchId = json['branch_id'];
    userId = json['user_id'];
    shiftDetailuserId = json['shift_detailuser_id'];
    content = json['content'];
    createBy = json['create_by'];
    updateBy = json['update_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    status = json['status'];
    fullNameReceiver = json['full_name_receiver'];
    avatarReceiver = json['avatar_receiver'];
    fullName = json['full_name'];
    avatar = json['avatar'];
    isMe = json['is_me'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['branch_id'] = this.branchId;
    data['user_id'] = this.userId;
    data['shift_detailuser_id'] = this.shiftDetailuserId;
    data['content'] = this.content;
    data['create_by'] = this.createBy;
    data['update_by'] = this.updateBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['status'] = this.status;
    data['full_name_receiver'] = this.fullNameReceiver;
    data['avatar_receiver'] = this.avatarReceiver;
    data['full_name'] = this.fullName;
    data['avatar'] = this.avatar;
    data['is_me'] = this.isMe;
    return data;
  }
}

class CountTotal {
  int status1;
  int status2;
  int status3;

  CountTotal({this.status1, this.status2, this.status3});

  CountTotal.fromJson(Map<String, dynamic> json) {
    status1 = json['status1'];
    status2 = json['status2'];
    status3 = json['status3'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status1'] = this.status1;
    data['status2'] = this.status2;
    data['status3'] = this.status3;
    return data;
  }
}
