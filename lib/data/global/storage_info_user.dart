import 'package:be_staff/data/response/user_response.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorageInfoUser {
  StorageInfoUser._internal();

  static final StorageInfoUser _storage = StorageInfoUser._internal();

  factory StorageInfoUser() => _storage;

  UserResponse _userInfo;

  void setInfoUser(UserResponse value) {
    _storage._userInfo = value;
  }

  UserResponse get getInfoUser {
    if (_storage._userInfo != null) {
      return _storage._userInfo;
    }
    return null;
  }

  Future<void> clearAll() async {
    _userInfo = null;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove("infoUser");
  }
}
