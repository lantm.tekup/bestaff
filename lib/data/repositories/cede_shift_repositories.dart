import 'package:be_staff/data/response/cede_shift_response.dart';

abstract class CedeShiftRepositories {
  Future<CedeShiftResponse> onGetCedeShift(int branchId, String date, int shiftDetailId);

  Future<String> onCedeShift(int shiftId, int positionId);
}