import 'dart:convert';

import 'package:be_staff/data/exception/network_exception.dart';
import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/overtime_repositories.dart';
import 'package:be_staff/data/request/overtime_request.dart';
import 'package:be_staff/data/response/information_overtime_response.dart';

class OvertimeRepositoriesImpl implements OvertimeRepositories {
  @override
  Future<String> onCreateOvertimeRequest(OvertimeRequest request) async {
    // TODO: implement onCreateOvertimeRequest
    final url = NetworkConfig.ADD_OVERTIME;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode(request.toJson());
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      if(response['success'] == 200){
        return response['comment'];
      }
      throw NetworkException(response['comment']);
    }
    throw NetworkException(response['comment']);
  }

  @override
  Future<InformationOvertimeResponse> onGetInformationOvertime() async {
    // TODO: implement onGetInformationOvertime
    final url = NetworkConfig.GET_INFORMATION_OVERTIME;
    final header = NetworkConfig.onBuildHeader();
    final responseJson = await NetworkClient.onGet(url, header: header);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return InformationOvertimeResponse.fromJson(response);
    }
    throw NetworkException();
  }
}
