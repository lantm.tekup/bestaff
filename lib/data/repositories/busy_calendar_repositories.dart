import 'package:be_staff/data/request/update_busy_calendar_request.dart';
import 'package:be_staff/data/response/busy_time_response.dart';

abstract class BusyCalendarRepositories {
  Future<BusyTimeResponse> onGetBusyTime();

  Future<String> onUpdateBusyCalendar(List<UpdateBusyCalendarRequest> request);
}
