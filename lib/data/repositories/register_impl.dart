import 'dart:convert';

import 'package:be_staff/data/exception/network_exception.dart';
import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/register_repositories.dart';
import 'package:be_staff/data/request/register_request.dart';
import 'package:be_staff/data/response/register_response.dart';

class RegisterImpl implements RegisterRepositories {
  @override
  Future<RegisterResponse> onRegister(RegisterRequest registerRequest) async {
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.REGISTER;
    final body = json.encode(registerRequest);
    final responseJson =
        await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if (responseJson.statusCode == 200) {
      return RegisterResponse.fromJson(response);
    }
    throw NetworkException(response['errors']['email'][0]);
  }

  @override
  Future<String> onEnterCode(String code) async {
    // TODO: implement onEnterCode
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.CODE_COMPANY;
    final body = json.encode({"code": code});
    final responseJson =
        await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    print('------------ $code --- ${responseJson.statusCode}');
    if (responseJson.statusCode == 200) {
      return response['result'];
    }
    throw Exception();
  }
}
