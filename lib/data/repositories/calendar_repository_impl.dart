import 'dart:convert';

import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/calendar_repositories.dart';
import 'package:be_staff/data/response/calendar_response.dart';

class CalendarRepositoryImpl implements CalendarRepositories {
  @override
  Future<CalendarResponse> onGetWorkingCalendar() async {
    // TODO: implement onGetWorkingCalendar
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.CALENDAR;
    final responseJson = await NetworkClient.onGet(url, header: header);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return CalendarResponse.fromJson(response);
    }
    throw Exception(response);
  }

}