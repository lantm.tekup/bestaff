import 'package:be_staff/data/models/user.dart';
import 'package:be_staff/data/request/login_request.dart';
import 'package:be_staff/data/response/user_response.dart';

abstract class LoginRepositories {
  Future<UserResponse> onLogin(LoginRequest request);
}