class CheckOutRequest {
  int userId;
  String image;
  int ipAddress;
  String location;
  int branchId;
  String token;
  int checkInByCode;

  CheckOutRequest(
      {this.userId,
        this.image,
        this.ipAddress,
        this.location,
        this.branchId,
        this.token,
        this.checkInByCode});

  CheckOutRequest.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    image = json['image'];
    ipAddress = json['ip_address'];
    location = json['location'];
    branchId = json['branch_id'];
    token = json['token'];
    checkInByCode = json['checkinByCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['image'] = this.image;
    data['ip_address'] = this.ipAddress;
    data['location'] = this.location;
    data['branch_id'] = this.branchId;
    data['token'] = this.token;
    data['checkinByCode'] = this.checkInByCode;
    return data;
  }
}
