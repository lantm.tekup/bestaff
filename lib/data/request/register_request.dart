class RegisterRequest {
  String fullName;
  String email;
  String password;
  String mobile;

  RegisterRequest({this.fullName, this.email, this.password, this.mobile});

  RegisterRequest.fromJson(Map<String, dynamic> json) {
    fullName = json['full_name'];
    email = json['email'];
    password = json['password'];
    mobile = json['mobile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['full_name'] = this.fullName;
    data['email'] = this.email;
    data['password'] = this.password;
    data['mobile'] = this.mobile;
    return data;
  }
}