import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/fonts.dart';
import 'package:flutter/material.dart';

import 'dimens.dart';

final TextStyle styleHintInput = TextStyle(
  fontWeight: FontWeight.w500,
    color: bdGrayColor,
    fontSize: mediumSize,
    fontFamily: fInterRegular);

final TextStyle styleInterRegular = TextStyle(
    fontWeight: FontWeight.w400,
    color: gray89Color,
    fontSize: smallSize,
    fontFamily: fInterRegular);
final TextStyle styleUnderlinedBoldBlack = TextStyle(
    fontWeight: FontWeight.w700,
    decoration: TextDecoration.underline,
    color: black,
    fontSize: smallSize,
    fontFamily: fInterRegular);

final TextStyle styleHintInputEmailMobile = TextStyle(
    color: gray66Color,
    fontSize: bigSize,
    fontFamily: fBarlowSemiBold);
