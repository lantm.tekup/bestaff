const String fBarlow = "Barlow";
const String fBarlowBlack = "Barlow-Black";
const String fBarlowMedium = "Barlow-Medium";
const String fInterRegular = "Inter-Regular";
const String fRobotoRegular = "Roboto-Regular";
const String fBarlowSemiBold = "Barlow-SemiBold";
const String fInterSemiBold = "Inter-SemiBold";